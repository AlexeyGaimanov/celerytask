from __future__ import unicode_literals

from django.db import models


class Object(models.Model):
    task_name = models.CharField(max_length=40, null=True)
    task_id = models.CharField(max_length=36,
                               blank=True, null=True)
    progress = models.DecimalField(default=0, decimal_places=4, max_digits=7)
