import random
from decimal import Decimal

from celery import group, chain
from celery.utils.log import get_task_logger
from django.db.models import F

from celerytask.celery import app
from .models import Object

TASK_B_NUMBER = 10
TASK_B_PERCENTAGE = Decimal('100') / Decimal(str(TASK_B_NUMBER))
# MAIN_CHAIN = chain([])

logger = get_task_logger(__name__)


@app.task(bind=True)
def task_a(self, *args, **kwargs):
    Object.objects.filter(pk=kwargs.get('obj_pk')).update(
        task_name='TASK A',
        task_id=self.request.id,
        progress=0
    )

    logger.debug(task_a.__name__)
    logger.debug(self.request.id)


@app.task(bind=True)
def task_b(self, *args, **kwargs):
    Object.objects.filter(pk=kwargs.get('obj_pk')).update(
        task_name='TASK B',
        task_id=self.request.id,
        progress=F('progress') + TASK_B_PERCENTAGE
    )

    logger.debug(task_b.__name__)
    logger.debug(self.request.id)


@app.task(bind=True)
def task_b2(self, *args, **kwargs):
    Object.objects.filter(pk=kwargs.get('obj_pk')).update(
        task_name='TASK B2',
        task_id=self.request.id,
        progress=0
    )

    task_c_number = random.randint(1000, 2000)
    task_c_percentage = str(Decimal('100') / Decimal(str(task_c_number)))

    logger.debug(task_b2.__name__)
    logger.debug(self.request.id)

    chain(
        group(task_c.s(obj_pk=kwargs['obj_pk'], task_c_percentage=task_c_percentage) for _ in xrange(task_c_number)),
        task_d.s(**kwargs)
    )()


@app.task(bind=True)
def task_c(self, *args, **kwargs):
    Object.objects.filter(pk=kwargs.get('obj_pk')).update(
        task_name='TASK C',
        task_id=self.request.id,
        progress=F('progress') + Decimal(kwargs['task_c_percentage'])
    )

    logger.debug(task_c.__name__)
    logger.debug(self.request.id)


@app.task(bind=True)
def task_d(self, *args, **kwargs):
    Object.objects.filter(pk=kwargs.get('obj_pk')).update(
        task_name='TASK D',
        task_id=self.request.id,
        progress=0
    )

    logger.debug(task_d.__name__)
    logger.debug(self.request.id)


@app.task(bind=True)
def task_bc(self, *args, **kwargs):
    Object.objects.filter(pk=kwargs.get('obj_pk')).update(
        task_name='TASK BC',
        task_id=self.request.id,
    )

    logger.debug(task_bc.__name__)
    logger.debug(self.request.id)


@app.task(bind=True)
def task_e(self, *args, **kwargs):
    Object.objects.filter(pk=kwargs.get('obj_pk')).update(
        task_name='TASK E',
        task_id=self.request.id,
    )

    logger.debug(task_e.__name__)
    logger.debug(self.request.id)

    for _ in xrange(40):
        task_bc.apply_async(kwargs=kwargs,
                            exchange='tasks_bc',
                            routing_key=random.choice(['tasks.task_b', 'tasks.task_c']))


@app.task()
def main_task():
    obj = Object.objects.create(
        task_name='MAIN',
        progress=0
    )

    signature = dict(obj_pk=obj.pk)

    group([chain(
            task_a.s(**signature),
            group(task_b.s(**signature) for _ in xrange(TASK_B_NUMBER)),
            task_b2.s(**signature)
    ), task_e.s(**signature)])()
